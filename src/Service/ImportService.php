<?php

namespace App\Service;

use App\Form\Model\UploadModel;
use App\Repository\CarRepository;
use Doctrine\DBAL\Exception;

class ImportService
{
    private CarRepository $carRepository;
    private CsvService $csvService;
    private SelectService $selectService;

    /**
     * @param CarRepository $carRepository
     * @param CsvService $csvService
     * @param SelectService $selectService
     */
    public function __construct(CarRepository $carRepository, CsvService $csvService, SelectService $selectService)
    {
        $this->carRepository = $carRepository;
        $this->csvService = $csvService;
        $this->selectService = $selectService;
    }

    /**
     * @param UploadModel $model
     * @return void
     */
    public function handler(UploadModel $model): void
    {
        $model->getFiles()->map(function ($file) {
            $this->csvService->makeNewFile($file);
            $this->importToDB();
            $this->csvService->deleteFile();
        });

        $this->selectService->updateAllDataForSelect();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function importToDB(): void
    {
        $this->carRepository->import($this->csvService->temporaryCsvFolder . CsvService::TEMPORARY_FILE_NAME);
    }
}