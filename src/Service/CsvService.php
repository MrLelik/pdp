<?php

namespace App\Service;

use App\Validator\TemporaryFileValidator;
use Generator;
use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\Reader;
use League\Csv\Writer;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CsvService
{
    private Writer $writer;
    private Reader $reader;
    private Filesystem $filesystem;
    private TemporaryFileValidator $validator;
    private array $records = [];
    private int $batchCount = 0;
    private int $batchSize = 2000;
    public string $temporaryCsvFolder;
    public const TEMPORARY_FILE_NAME = 'temporary_file.csv';
    private const FIELD_NAMES_TYPE_1 = [
        'BRAND' => 'BRAND',
        'COLOR' => 'COLOR',
        'KIND' => 'KIND',
        'BODY' => 'BODY',
        'PURPOSE' => 'PURPOSE',
        'MODEL' => 'MODEL',
        'FUEL' => 'FUEL',
        'MAKE_YEAR' => 'MAKE_YEAR',
        'DEP' => 'DEP',
        'DEP_CODE' => 'DEP_CODE',
    ];
    private const FIELD_NAMES_TYPE_2 = [
        'BRAND' => 'brand',
        'COLOR' => 'color',
        'KIND' => 'kind',
        'BODY' => 'body',
        'PURPOSE' => 'purpose',
        'MODEL' => 'model',
        'FUEL' => 'fuel',
        'MAKE_YEAR' => 'make_year',
        'DEP' => 'dep',
        'DEP_CODE' => 'dep_code',
    ];

    /**
     * @param string $temporaryCsvFolder
     * @param Filesystem $filesystem
     * @param TemporaryFileValidator $validator
     */
    public function __construct(string $temporaryCsvFolder, Filesystem $filesystem, TemporaryFileValidator $validator)
    {
        $this->temporaryCsvFolder = $temporaryCsvFolder;
        $this->filesystem = $filesystem;
        $this->validator = $validator;
    }

    /**
     * @param UploadedFile $file
     * @return void
     * @throws Exception
     */
    public function makeNewFile(UploadedFile $file): void
    {
        $this->initWriter();
        $this->initReader($file->getRealPath());
        $fields = $this->getFieldNamesType();

        foreach ($this->generator($this->reader->getRecords()) as $record) {
            try {
                $newLine = [
                    $this->validator->validateBrand($record[$fields['BRAND']]),
                    $this->validator->validateColor($record[$fields['COLOR']]),
                    $this->validator->validateKind($record[$fields['KIND']]),
                    $this->validator->validateBody($record[$fields['BODY']]),
                    $this->validator->validatePurpose($record[$fields['PURPOSE']]),
                    $this->validator->validateModel($record[$fields['MODEL']]),
                    $this->validator->validateFuel($record[$fields['FUEL']]),
                    $this->validator->validateMakeYear($record[$fields['MAKE_YEAR']]),
                    $this->validator->validateDep($record[$fields['DEP']]),
                    $this->validator->validateDepCode($record[$fields['DEP_CODE']]),
                ];
            } catch (Exception $exception) {
                $this->deleteFile();
                throw new Exception($exception->getMessage());
            }

            $this->records[] = $newLine;
            $this->increaseBatchCount();

            if ($this->checkBatchCount()) {
                $this->insertHandler();
            }
        }

        $this->insertHandler();
    }

    /**
     * @return void
     */
    public function deleteFile(): void
    {
        $this->filesystem->remove($this->temporaryCsvFolder . self::TEMPORARY_FILE_NAME);
    }

    /**
     * @return string[]
     */
    private function getFieldNamesType(): array
    {
        if (($header = $this->reader->getHeader()) && in_array(self::FIELD_NAMES_TYPE_1['BRAND'], $header)) {
            return self::FIELD_NAMES_TYPE_1;
        }

        return self::FIELD_NAMES_TYPE_2;
    }

    /**
     * @return void
     */
    private function insertHandler(): void
    {
        $this->insert();
        $this->resetBatchCount();
        $this->resetRecords();
    }

    /**
     * @return void
     */
    private function insert(): void
    {
        $this->writer->insertAll($this->records);
    }

    /**
     * @param string $path
     * @return void
     * @throws Exception
     * @throws InvalidArgument
     */
    private function initReader(string $path): void
    {
        $this->reader = Reader::createFromPath($path)->setHeaderOffset(0)->setDelimiter(';');
    }

    /**
     * @return void
     */
    private function initWriter(): void
    {
        $this->filesystem->dumpFile($this->temporaryCsvFolder . self::TEMPORARY_FILE_NAME, '');
        $this->writer = Writer::createFromPath($this->temporaryCsvFolder . self::TEMPORARY_FILE_NAME);
    }

    /**
     * @return bool
     */
    private function checkBatchCount(): bool
    {
        return $this->batchCount > $this->batchSize;
    }

    /**
     * @return void
     */
    private function increaseBatchCount(): void
    {
        $this->batchCount++;
    }

    /**
     * @return void
     */
    private function resetBatchCount(): void
    {
        $this->batchCount = 0;
    }

    /**
     * @return void
     */
    private function resetRecords(): void
    {
        $this->records = [];
    }

    /**
     * @param object $records
     * @return Generator
     */
    private function generator(object $records): Generator
    {
        foreach ($records as $record) {
            yield $record;
        }
    }
}