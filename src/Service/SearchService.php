<?php

namespace App\Service;

use App\Form\Model\SearchModel;
use App\Repository\CarRepository;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class SearchService
{
    private CarRepository $carRepository;
    private PaginatorInterface $paginator;
    private const RESULT_LIMIT = 12;

    /**
     * @param CarRepository $carRepository
     * @param PaginatorInterface $paginator
     */
    public function __construct(CarRepository $carRepository, PaginatorInterface $paginator)
    {
        $this->carRepository = $carRepository;
        $this->paginator = $paginator;
    }

    /**
     * @param SearchModel $searchModel
     * @param int $page
     * @return PaginationInterface
     */
    public function search(SearchModel $searchModel, int $page): PaginationInterface
    {
        return $this->paginate(
            $this->getQuerySearch($searchModel),
            $page
        );
    }

    /**
     * @param SearchModel $searchModel
     * @return QueryBuilder
     */
    public function getQuerySearch(SearchModel $searchModel): QueryBuilder
    {
        return $this->carRepository->search($searchModel);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param int $page
     * @return PaginationInterface
     */
    private function paginate(QueryBuilder $queryBuilder, int $page): PaginationInterface
    {
        return $this->paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            self::RESULT_LIMIT
        );
    }
}