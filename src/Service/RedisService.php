<?php

namespace App\Service;

use Redis;

class RedisService
{
    private Redis $redis;
    private const REDIS_HOST = 'redis';
    private const TIMEOUT_ONE_DAY = 60 * 60 * 24;

    public function __construct()
    {
        $this->redis = new Redis();
        $this->redis->connect(self::REDIS_HOST);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return $this->redis->exists($key);
    }

    /**
     * @param string $key
     * @return string
     */
    public function getString(string $key): string
    {
        return $this->redis->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setString(string $key, mixed $value): void
    {
        $this->redis->set($key ,$value, self::TIMEOUT_ONE_DAY);
    }

    /**
     * @param string $key
     * @return array
     */
    public function getArray(string $key): array
    {
        return json_decode($this->redis->get($key));
    }

    /**
     * @param string $key
     * @param array $array
     * @return void
     */
    public function setArray(string $key, array $array): void
    {
        $this->redis->set($key, json_encode($array), self::TIMEOUT_ONE_DAY);
    }
}