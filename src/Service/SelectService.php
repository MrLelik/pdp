<?php

namespace App\Service;

use App\Repository\CarRepository;

class SelectService
{
    private CarRepository $carRepository;
    private RedisService $redisService;
    private const REDIS_KEYS = [
        'BRAND' => 'brand',
        'COLOR' => 'color',
        'KIND' => 'kind',
        'BODY' => 'body',
        'PURPOSE' => 'purpose',
    ];

    /**
     * @param CarRepository $carRepository
     * @param RedisService $redisService
     */
    public function __construct(CarRepository $carRepository, RedisService $redisService)
    {
        $this->carRepository = $carRepository;
        $this->redisService = $redisService;
    }

    /**
     * @return void
     */
    public function updateAllDataForSelect(): void
    {
        foreach (self::REDIS_KEYS as $key) {
            $this->redisService->setArray($key, $this->carRepository->getUniqueValues($key));
        }
    }

    /**
     * @return array
     */
    public function getBrands(): array
    {
        return $this->getDataForSelect(self::REDIS_KEYS['BRAND']);
    }

    /**
     * @return array
     */
    public function getColors(): array
    {
        return $this->getDataForSelect(self::REDIS_KEYS['COLOR']);
    }

    /**
     * @return array
     */
    public function getKinds(): array
    {
        return $this->getDataForSelect(self::REDIS_KEYS['KIND']);
    }

    /**
     * @return array
     */
    public function getBodys(): array
    {
        return $this->getDataForSelect(self::REDIS_KEYS['BODY']);
    }

    /**
     * @return array
     */
    public function getPurposes(): array
    {
        return $this->getDataForSelect(self::REDIS_KEYS['PURPOSE']);
    }

    /**
     * @param string $key
     * @return array
     */
    private function getDataForSelect(string $key): array
    {
        if (!$this->redisService->has($key)) {
            $this->redisService->setArray($key, $dataSelect = $this->carRepository->getUniqueValues($key));
        }

        $dataSelect = $dataSelect ?? $this->redisService->getArray($key);

        return array_combine($dataSelect, $dataSelect);
    }
}