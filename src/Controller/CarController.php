<?php

namespace App\Controller;

use App\Repository\CarRepository;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/car")
 */
class CarController extends AbstractController
{
    private CarRepository $carRepository;

    /**
     * @param CarRepository $carRepository
     */
    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * @Route("/truncate", name="truncate")
     * @throws Exception
     */
    public function truncate(): Response
    {
        $this->carRepository->truncate();
        $this->addFlash('success', 'DB cleared');

        return $this->redirectToRoute('upload');
    }
}
