<?php

namespace App\Controller;

use App\Form\Model\UploadModel;
use App\Form\UploadType;
use App\Service\ImportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{
    private ImportService $importService;

    /**
     * @param ImportService $importService
     */
    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function upload(Request $request): Response
    {
        $uploadModel = new UploadModel();
        $form = $this->createForm(UploadType::class, $uploadModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->importService->handler($uploadModel);
            $this->addFlash('success', 'DB updated');

            $form = $this->createForm(UploadType::class, new UploadModel());
        }

        return $this->render('upload/upload.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
