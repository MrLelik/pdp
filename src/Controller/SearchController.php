<?php

namespace App\Controller;

use App\Form\Model\SearchModel;
use App\Form\SearchType;
use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private SearchService $searchService;

    /**
     * @param SearchService $searchService
     */
    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * @Route("/", name="search")
     */
    public function index(Request $request): Response
    {
        $searchModel = new SearchModel();
        $form = $this->createForm(SearchType::class, $searchModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cars = $this->searchService->search($searchModel, $request->query->getInt('page', 1));
        }

        return $this->render('search/search.html.twig', [
            'form' => $form->createView(),
            'cars' => $cars ?? [],
        ]);
    }
}
