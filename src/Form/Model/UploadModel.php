<?php

namespace App\Form\Model;

use Doctrine\Common\Collections\ArrayCollection;

class UploadModel
{
    private ArrayCollection $files;

    /**
     * @return ArrayCollection
     */
    public function getFiles(): ArrayCollection
    {
        return $this->files;
    }

    /**
     * @param array $files
     * @return $this
     */
    public function setFiles(array $files): self
    {
        $this->files = new ArrayCollection($files);

        return $this;
    }
}