<?php

namespace App\Form;

use App\Form\Model\UploadModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;

class UploadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('files', FileType::class, [
                'multiple' => true,
                'required' => true,
                'label' => 'Choose file or files',
                'attr' => [
                    'class' => 'custom-file-input form-control',
                    'accept' => '.csv',
                ],
                'label_attr' => [
                    'class' => 'custom-file-label',
                ],
                'constraints' => [
                    new All([
                        new File([
                            'maxSize' => '900M',
                            'mimeTypes' => [
                                'text/csv',
                                'application/csv',
                                'text/plain',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid CSV file',
                        ]),
                    ]),
                ],
            ])
            ->add('button', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-block btn-dark',
                ],
                'label' => 'Upload',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UploadModel::class,
            'attr' => [
                'id' => 'upload-form',
            ],
        ]);
    }
}
