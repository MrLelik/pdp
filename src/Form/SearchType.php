<?php

namespace App\Form;

use App\Form\Model\SearchModel;
use App\Service\SelectService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    private SelectService $selectService;

    /**
     * @param SelectService $selectService
     */
    public function __construct(SelectService $selectService)
    {
        $this->selectService = $selectService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('general', TextType::class, [
                'required' => false,
            ])
            ->add('brand', ChoiceType::class, [
                'choices' => $this->selectService->getBrands(),
                'attr' => [
                    'class' => 'form-control',
                ],
                'placeholder' => 'Choose an option',
                'required' => false,
            ])
            ->add('color', ChoiceType::class, [
                'choices' => $this->selectService->getColors(),
                'attr' => [
                    'class' => 'form-control',
                ],
                'placeholder' => 'Choose an option',
                'required' => false,
            ])
            ->add('kind', ChoiceType::class, [
                'choices' => $this->selectService->getKinds(),
                'attr' => [
                    'class' => 'form-control',
                ],
                'placeholder' => 'Choose an option',
                'required' => false,
            ])
            ->add('body', ChoiceType::class, [
                'choices' => $this->selectService->getBodys(),
                'attr' => [
                    'class' => 'form-control',
                ],
                'placeholder' => 'Choose an option',
                'required' => false,
            ])
            ->add('purpose', ChoiceType::class, [
                'choices' => $this->selectService->getPurposes(),
                'attr' => [
                    'class' => 'form-control',
                ],
                'placeholder' => 'Choose an option',
                'required' => false,
            ])
            ->add('search', SubmitType::class, [
                'label' => "Find a Car",
                'attr' => [
                    'class' => 'btn btn-primary py-5 py-md-3 px-4 align-self-stretch d-block',
                ],
            ])
            ->setMethod('GET');
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchModel::class,
            'attr' => [
                'class' => 'booking-form',
            ],
        ]);
    }
}
