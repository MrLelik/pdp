<?php

namespace App\Logger;

use Psr\Log\LoggerInterface;

abstract class BaseLogger
{
    protected LoggerInterface $logger;

    /**
     * @param LoggerInterface $customLogger
     */
    public function __construct(LoggerInterface $customLogger)
    {
        $this->logger = $customLogger;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}