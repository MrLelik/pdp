<?php

namespace App\Repository;

use App\Entity\Car;
use App\Form\Model\SearchModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function truncate(): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'TRUNCATE TABLE cars';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery();
    }

    /**
     * @param string $path
     * @return void
     * @throws Exception
     */
    public function import(string $path): void
    {
        $connection = $this->_em->getConnection();
        $sql = "LOAD DATA LOCAL INFILE '$path'
                INTO TABLE cars CHARACTER SET utf8
                FIELDS 
                    TERMINATED BY ','
                    ENCLOSED BY '\"'
                    ESCAPED BY '\"'
                LINES 
                    TERMINATED BY '\n'
                IGNORE 1 LINES 
                (brand, color, kind, body, purpose, model, fuel, make_year, dep, dep_code)";

        $smtp = $connection->prepare($sql);
        $smtp->executeQuery();
    }

    /**
     * @param string $field
     * @return array
     */
    public function getUniqueValues(string $field): array
    {
        return $this->createQueryBuilder('c')
            ->select("c.$field")
            ->distinct(true)
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * @param SearchModel $searchModel
     * @return QueryBuilder
     */
    public function search(SearchModel $searchModel): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');

        if ($general = $searchModel->getGeneral()) {
            $qb
                ->orWhere('c.model LIKE :model')->setParameter('model', '%' . $general . '%')
                ->orWhere('c.fuel LIKE :fuel')->setParameter('fuel', '%' . $general . '%')
                ->orWhere('c.makeYear LIKE :year')->setParameter('year', '%' . $general . '%')
                ->orWhere('c.dep LIKE :dep')->setParameter('dep', '%' . $general . '%')
                ->orWhere('c.depCode LIKE :depCode')->setParameter('depCode', '%' . $general . '%');
        }

        if ($brand = $searchModel->getBrand()) {
            $qb->andWhere('c.brand = :brand')->setParameter('brand', $brand);
        }

        if ($color = $searchModel->getColor()) {
            $qb->andWhere('c.color = :color')->setParameter('color', $color);
        }

        if ($kind = $searchModel->getKind()) {
            $qb->andWhere('c.kind = :kind')->setParameter('kind', $kind);
        }

        if ($body = $searchModel->getBody()) {
            $qb->andWhere('c.body = :body')->setParameter('body', $body);
        }

        if ($purpose = $searchModel->getPurpose()) {
            $qb->andWhere('c.purpose = :purpose')->setParameter('purpose', $purpose);
        }

        return $qb;
    }
}
