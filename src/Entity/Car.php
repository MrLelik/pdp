<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 * @ORM\Table(
 *     name="cars",
 *     indexes={@ORM\Index(
 *     name="search_idx", columns={"brand", "color", "kind", "body", "purpose"}
 *     )}
 *     )
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=44, nullable=true)
     */
    private ?string $brand;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    private ?string $color;

    /**
     * @ORM\Column(type="string", length=28, nullable=true)
     */
    private ?string $kind;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private ?string $body;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $purpose;

    /**
     * @ORM\Column(type="string", length=39, nullable=true)
     */
    private ?string $model;

    /**
     * @ORM\Column(type="string", length=51, nullable=true)
     */
    private ?string $fuel;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $makeYear;

    /**
     * @ORM\Column(type="text", length=268, nullable=true)
     */
    private ?string $dep;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $depCode;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string|null $brand
     * @return $this
     */
    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     * @return $this
     */
    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKind(): ?string
    {
        return $this->kind;
    }

    /**
     * @param string|null $kind
     * @return $this
     */
    public function setKind(?string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string|null $body
     * @return $this
     */
    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPurpose(): ?string
    {
        return $this->purpose;
    }

    /**
     * @param string|null $purpose
     * @return $this
     */
    public function setPurpose(?string $purpose): self
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string|null $model
     * @return $this
     */
    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFuel(): ?string
    {
        return $this->fuel;
    }

    /**
     * @param string|null $fuel
     * @return $this
     */
    public function setFuel(?string $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMakeYear(): ?int
    {
        return $this->makeYear;
    }

    /**
     * @param int|null $makeYear
     * @return $this
     */
    public function setMakeYear(?int $makeYear): self
    {
        $this->makeYear = $makeYear;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDep(): ?string
    {
        return $this->dep;
    }

    /**
     * @param string|null $dep
     * @return $this
     */
    public function setDep(?string $dep): self
    {
        $this->dep = $dep;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDepCode(): ?int
    {
        return $this->depCode;
    }

    /**
     * @param int|null $depCode
     * @return $this
     */
    public function setDepCode(?int $depCode): self
    {
        $this->depCode = $depCode;

        return $this;
    }
}
