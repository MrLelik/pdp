<?php

namespace App\Validator;

use App\Logger\WriterLogger;
use League\Csv\Exception;

class TemporaryFileValidator
{
    private WriterLogger $writerLogger;
    public const CHECKED_LENGTH = [
        'BRAND' => 44,
        'COLOR' => 24,
        'KIND' => 28,
        'BODY' => 70,
        'PURPOSE' => 30,
        'MODEL' => 39,
        'FUEL' => 51,
        'MAKE_YEAR' => 4,
        'DEP' => 268,
        'DEP_CODE_MIN' => 3,
        'DEP_CODE_MAX' => 8,
    ];

    /**
     * @param WriterLogger $writerLogger
     */
    public function __construct(WriterLogger $writerLogger)
    {
        $this->writerLogger = $writerLogger;
    }

    /**
     * @param string $error
     * @return void
     */
    private function writeError(string $error): void
    {
        $this->writerLogger->getLogger()->error($error);
    }

    /**
     * @param $brand
     * @return string
     * @throws Exception
     */
    public function validateBrand($brand): string
    {
        if (!is_string($brand) || strlen($brand) > self::CHECKED_LENGTH['BRAND']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Brand expected length %d we got %s',
                    self::CHECKED_LENGTH['BRAND'],
                    strlen($brand)
                ));
            throw new Exception($message);
        }

        return $brand;
    }

    /**
     * @param $color
     * @return string
     * @throws Exception
     */
    public function validateColor($color): string
    {
        if (!is_string($color) || strlen($color) > self::CHECKED_LENGTH['COLOR']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Color expected length %d we got %s',
                    self::CHECKED_LENGTH['COLOR'],
                    strlen($color)
                ));
            throw new Exception($message);
        }

        return $color;
    }

    /**
     * @param $kind
     * @return string
     * @throws Exception
     */
    public function validateKind($kind): string
    {
        if (!is_string($kind) || strlen($kind) > self::CHECKED_LENGTH['KIND']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Kind expected length %d we got %s',
                    self::CHECKED_LENGTH['KIND'],
                    strlen($kind)
                ));
            throw new Exception($message);
        }

        return $kind;
    }

    /**
     * @param $body
     * @return string
     * @throws Exception
     */
    public function validateBody($body): string
    {
        if (!is_string($body) || strlen($body) > self::CHECKED_LENGTH['BODY']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Body expected length %d we got %s',
                    self::CHECKED_LENGTH['BODY'],
                    strlen($body)
                ));
            throw new Exception($message);
        }

        return $body;
    }

    /**
     * @param $purpose
     * @return string
     * @throws Exception
     */
    public function validatePurpose($purpose): string
    {
        if (!is_string($purpose) || strlen($purpose) > self::CHECKED_LENGTH['PURPOSE']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Purpose expected length %d we got %s',
                    self::CHECKED_LENGTH['PURPOSE'],
                    strlen($purpose)
                ));
            throw new Exception($message);
        }

        return $purpose;
    }

    /**
     * @param $model
     * @return string
     * @throws Exception
     */
    public function validateModel($model): string
    {
        if (!is_string($model) || strlen($model) > self::CHECKED_LENGTH['MODEL']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Model expected length %d we got %s',
                    self::CHECKED_LENGTH['MODEL'],
                    strlen($model)
                ));
            throw new Exception($message);
        }

        return $model;
    }

    /**
     * @param $fuel
     * @return string
     * @throws Exception
     */
    public function validateFuel($fuel): string
    {
        if (!is_string($fuel) || strlen($fuel) > self::CHECKED_LENGTH['FUEL']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Fuel expected length %d we got %s',
                    self::CHECKED_LENGTH['FUEL'],
                    strlen($fuel)
                ));
            throw new Exception($message);
        }

        return $fuel;
    }

    /**
     * @param $year
     * @return string
     * @throws Exception
     */
    public function validateMakeYear($year): string
    {
        $year = (int)$year;

        if (strlen((string)$year) !== self::CHECKED_LENGTH['MAKE_YEAR']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Make Year expected length %d we got %s',
                    self::CHECKED_LENGTH['MAKE_YEAR'],
                    strlen($year)
                ));
            throw new Exception($message);
        }

        return $year;
    }

    /**
     * @param $dep
     * @return string
     * @throws Exception
     */
    public function validateDep($dep): string
    {
        if (!is_string($dep) || strlen($dep) > self::CHECKED_LENGTH['DEP']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Dep expected length %d we got %s',
                    self::CHECKED_LENGTH['DEP'],
                    strlen($dep)
                ));
            throw new Exception($message);
        }

        return $dep;
    }

    /**
     * @param $depCode
     * @return string
     * @throws Exception
     */
    public function validateDepCode($depCode): string
    {
        $depCode = (int)$depCode;

        if (strlen((string)$depCode) < self::CHECKED_LENGTH['DEP_CODE_MIN'] ||
            strlen((string)$depCode) > self::CHECKED_LENGTH['DEP_CODE_MAX']) {
            $this->writeError(
                $message = sprintf(
                    'Error validate Dep Code expected length %d or %t we got %s',
                    self::CHECKED_LENGTH['DEP_CODE_MIN'],
                    self::CHECKED_LENGTH['DEP_CODE_MAX'],
                    strlen($depCode)
                ));
            throw new Exception($message);
        }

        return $depCode;
    }
}