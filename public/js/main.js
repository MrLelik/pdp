$(document).ready(function () {
    $("#upload-form").submit(function () {
        $("#preloader").attr("hidden", false);
        startLoaderTimer();
    });

    $('input[type="file"]').on("change", function() {
        let filenames = [];
        let files = this.files;

        if (files.length > 1) {
            filenames.push("Total Files (" + files.length + ")");
        } else {
            for (let i in files) {
                if (files.hasOwnProperty(i)) {
                    filenames.push(files[i].name);
                }
            }
        }
        $('.custom-file-label').html(filenames.join(","));
    });

    $('.hide-element').fadeOut(6000);
});

function startLoaderTimer() {
    let sec = 0;
    function pad ( val ) { return val > 9 ? val : "0" + val; }
    setInterval( function(){
        $("#seconds").html(pad(++sec%60));
        $("#minutes").html(pad(parseInt(sec/60,10)));
    }, 1000);
}