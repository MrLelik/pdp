#!/usr/bin/env sh
setPermissions() {
    mkdir public/csvFiles
    mkdir public/temporaryCsv
    chmod -R 777 var
    chmod -R 777 public/csvFiles
    chmod -R 777 public/temporaryCsv
}

runComposer() {
    COMPOSER_MEMORY_LIMIT=-1 composer install --optimize-autoloader && COMPOSER_MEMORY_LIMIT=-1 composer dump-autoload --optimize
}

runComposer
setPermissions
exec php-fpm;