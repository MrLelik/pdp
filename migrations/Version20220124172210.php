<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220124172210 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE cars (id INT AUTO_INCREMENT NOT NULL, brand VARCHAR(44) DEFAULT NULL, color VARCHAR(24) DEFAULT NULL, kind VARCHAR(28) DEFAULT NULL, body VARCHAR(70) DEFAULT NULL, purpose VARCHAR(30) DEFAULT NULL, model VARCHAR(39) DEFAULT NULL, fuel VARCHAR(51) DEFAULT NULL, make_year INT DEFAULT NULL, dep TEXT DEFAULT NULL, dep_code INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE cars');
    }
}
