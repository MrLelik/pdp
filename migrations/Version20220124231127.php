<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220124231127 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX search_idx ON cars (brand, color, kind, body, purpose)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX search_idx ON cars');
    }
}
