<?php

namespace App\Tests\Repository;

use App\Entity\Car;
use App\Form\Model\SearchModel;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CarRepositoryTest extends KernelTestCase
{
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @return void
     */
    public function testSearch(): void
    {
        $searchModel = (new SearchModel())
            ->setGeneral('General text')
            ->setBrand('SKODA  OCTAVIA')
            ->setColor('BLACK')
            ->setKind('LIGHTWEIGHT')
            ->setBody('SEDAN')
            ->setPurpose('GENERAL');

        $queryBuilder = $this->entityManager->getRepository(Car::class)->search($searchModel);

        $this->assertEquals(
            "SELECT c FROM App\Entity\Car c WHERE (c.model LIKE :model OR c.fuel LIKE :fuel OR c.makeYear LIKE :year OR c.dep LIKE :dep OR c.depCode LIKE :depCode) AND c.brand = :brand AND c.color = :color AND c.kind = :kind AND c.body = :body AND c.purpose = :purpose",
            $queryBuilder->getDQL()
        );
        $this->assertEquals('%'.$searchModel->getGeneral().'%', $queryBuilder->getParameter('model')->getValue());
        $this->assertEquals('%'.$searchModel->getGeneral().'%', $queryBuilder->getParameter('fuel')->getValue());
        $this->assertEquals('%'.$searchModel->getGeneral().'%', $queryBuilder->getParameter('year')->getValue());
        $this->assertEquals('%'.$searchModel->getGeneral().'%', $queryBuilder->getParameter('dep')->getValue());
        $this->assertEquals('%'.$searchModel->getGeneral().'%', $queryBuilder->getParameter('depCode')->getValue());
        $this->assertEquals($searchModel->getBrand(), $queryBuilder->getParameter('brand')->getValue());
        $this->assertEquals($searchModel->getColor(), $queryBuilder->getParameter('color')->getValue());
        $this->assertEquals($searchModel->getKind(), $queryBuilder->getParameter('kind')->getValue());
        $this->assertEquals($searchModel->getBody(), $queryBuilder->getParameter('body')->getValue());
        $this->assertEquals($searchModel->getPurpose(), $queryBuilder->getParameter('purpose')->getValue());
    }
}