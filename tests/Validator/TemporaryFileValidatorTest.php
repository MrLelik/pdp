<?php

namespace App\Tests\Validator;

use App\Logger\WriterLogger;
use App\Validator\TemporaryFileValidator;
use Exception as BytesException;
use League\Csv\Exception;
use PHPUnit\Framework\TestCase;

class TemporaryFileValidatorTest extends TestCase
{
    private TemporaryFileValidator $temporaryFileValidator;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->temporaryFileValidator = new TemporaryFileValidator($this->createMock(WriterLogger::class));
    }

    /**
     * @throws BytesException
     */
    private function generateString(int $length): string
    {
        return random_bytes($length);
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueBrand(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['BRAND']),
            $this->temporaryFileValidator->validateBrand($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueColor(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['COLOR']),
            $this->temporaryFileValidator->validateColor($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueKind(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['KIND']),
            $this->temporaryFileValidator->validateKind($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueBody(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['BODY']),
            $this->temporaryFileValidator->validateBody($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTruePurpose(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['PURPOSE']),
            $this->temporaryFileValidator->validatePurpose($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueModel(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['MODEL']),
            $this->temporaryFileValidator->validateModel($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueFuel(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['FUEL']),
            $this->temporaryFileValidator->validateFuel($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueMakeYear(): void
    {
        $this->assertEquals(
            '1234',
            $this->temporaryFileValidator->validateMakeYear('1234')
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueDep(): void
    {
        $this->assertEquals(
            $value = $this->generateString(TemporaryFileValidator::CHECKED_LENGTH['DEP']),
            $this->temporaryFileValidator->validateDep($value)
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateTrueDepCode(): void
    {
        $this->assertEquals(
            '1234',
            $this->temporaryFileValidator->validateDepCode('1234')
        );
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseBrand(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateBrand($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['BRAND'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseColor(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateColor($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['COLOR'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseKind(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateKind($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['KIND'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseBody(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateBody($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['BODY'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalsePurpose(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validatePurpose($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['PURPOSE'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseModel(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateModel($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['MODEL'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseFuel(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateFuel($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['FUEL'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseMakeYear(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator->validateMakeYear('12345');
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseDep(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateDep($this->generateString(TemporaryFileValidator::CHECKED_LENGTH['DEP'] + 1));
    }

    /**
     * @return void
     * @throws Exception
     * @throws BytesException
     */
    public function testValidateFalseDepCode(): void
    {
        $this->expectException(Exception::class);
        $this->temporaryFileValidator
            ->validateDepCode('123456789');
    }
}